"use strict";

const path = require("path");
const isLocal = typeof process.pkg === "undefined";
const basePath = isLocal ? process.cwd() : path.dirname(process.execPath);
const { MODE } = require(path.join(basePath, "constants/blend_mode.js"));
const description = "This is the description";
const baseUri = "image.png";

const layerConfigurations = [
	{
		growEditionSizeTo: 20,
		layersOrder: [{ name: "Background" }],
	},
];

const shuffleLayerConfigurations = false;

const debugLogs = false;

const format = {
	width: 1024,
	height: 1024,
};

const text = {
	color: "#ffffff",
	size: 25,
	xGap: 512,
	yGap: 512,
	align: "center",
	baseline: "center",
	weight: "regular",
	family: ["Arial", "Roboto", "Monospace", "Sans-serif", "Montserrat", "Times New Roman", "Helvetica", "Calibri", "Futura", "Garamond", "Verdana", "Cambria", "Baskerville", "Gotham", "Bodoni", "Didot", "Rockwell", "Mrs Eaves"],
};

const pixelFormat = {
	ratio: 2 / 128,
};

const background = {
	generate: true,
	brightness: "50%",
	static: false,
	default: "#000000",
};

const extraMetadata = {};

const rarityDelimiter = "#";

const uniqueDnaTorrance = 10000;

const preview = {
	thumbPerRow: 5,
	thumbWidth: 50,
	imageRatio: format.width / format.height,
	imageName: "preview.png",
};

module.exports = {
	format,
	baseUri,
	description,
	background,
	uniqueDnaTorrance,
	layerConfigurations,
	rarityDelimiter,
	preview,
	shuffleLayerConfigurations,
	debugLogs,
	extraMetadata,
	pixelFormat,
	text,
};
